# Copyright 2009-2012 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PN}${MY_PV}

require ant java

export_exlib_phases src_prepare src_install

SUMMARY="Open source JavaScript engine"
DESCRIPTION="
Rhino is an open-source implementation of JavaScript written entirely in Java. It
is typically embedded into Java applications to provide scripting to end users.
"
HOMEPAGE="http://www.mozilla.org/${PN}/"
DOWNLOADS="https://github.com/downloads/mozilla/${PN}/${MY_PNV}.zip"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:${PN}"

UPSTREAM_DOCUMENTATION="https://developer.mozilla.org/en/Rhino_documentation [[ lang = en ]]"

LICENCES="MPL-2.0 BSD-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-arch/unzip
        dev-java/jsr173[>=1.0]
        dev-java/xmlbeans-bin[>=2.4.0]
"

WORK=${WORKBASE}/${MY_PNV}

ANT_SRC_PREPARE_PARAMS=( deepclean )
ANT_SRC_COMPILE_PARAMS=( compile-all )
ANT_SRC_INSTALL_PARAMS=( jar )

rhino_src_prepare() {
    ant_src_prepare

    edo sed -i -e 's:\(<target name="old-e4x-compile"\).*:\1>:' xmlimplsrc/build.xml

    edo mkdir "${WORK}"/lib
    local jsr_ver=$(best_version dev-java/jsr173)
    jsr_ver=${jsr_ver%%:*}
    jsr_ver=${jsr_ver/*\/}

    edo ln -s /usr/share/jsr173/${jsr_ver%%-*}.jar "${WORK}"/lib/jsr173_1.0_api.jar
    edo ln -s /usr/share/xmlbeans/lib/xbean.jar "${WORK}"/lib/
}

rhino_src_install() {
    ant_src_install

    dodir /usr/share/${PN}
    insinto /usr/share/${PN}
    doins "${WORK}"/build/rhino${MY_PV}/js.jar
}

